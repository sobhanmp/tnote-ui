import React from 'react';
import { connect } from 'react-redux';

import { pushCutSet, popCutSet } from '../actions'

import { Group } from '@vx/group';
import { Treemap } from '@vx/hierarchy';
import { hierarchy, stratify } from 'd3-hierarchy';
import { shakespeare } from '@vx/mock-data';
import { treemapSquarify } from 'd3-hierarchy';
import { scaleLinear } from '@vx/scale';

const peach = '#fd9b93';
const pink = '#fe6e9e';
const blue = '#03c0dc';
const green = '#26deb0';
const plum = '#71248e';
const lightpurple = '#374469';
const white = '#ffffff';
const bg = '#272b4d';


const mapStateToProps = state => ({
  datum :  state.tree.cutSet.slice(-1)[0]
})
const mapDispatchToProps = dispatch => ({
  pushCutSet: c => dispatch(pushCutSet(c)),
  popCutSet: () => dispatch(popCutSet())

})

export default connect
  (mapStateToProps,
  mapDispatchToProps)(({
  datum,
  width,
  height,

  pushCutSet,
  popCutSet,
  margin = {
    top: 0,
    left: 30,
    right: 40,
    bottom: 80
  }
}) => {

  console.log("datum")
  console.log(datum)
  var t = stratify()
    .id(d => d.id)
    .parentId(d=> {return d.parentTaskId})

  var nd = datum.map(e => ({...e, size: 100}))
  console.log("new datum")
  console.log(nd)
  t = t(nd)
    .sum(d => d.size || 0)

  const colorScale = scaleLinear({
    domain: [0, datum.length*20],
    range: [blue, green]
  });

  const yMax = height - margin.top - margin.bottom;
  const root = hierarchy(t).sort((a, b) => b.value - a.value);

  return (
    <svg width={width} height={height}>

      <rect width={width} height={height} fill={bg} />
      <Treemap
        root={root}
        size={[width, yMax]}
        tile={treemapSquarify}
        round={true}
      >

        {treemap => {
          const nodes = treemap.descendants().reverse();
          return (
            <Group top={margin.top}>
              {nodes.map((node, i) => {
                const width = node.x1 - node.x0;
                const height = node.y1 - node.y0;
                return (
                  <Group key={`treemap-node-${i}`}
                    top={node.y0} left={node.x0}>
                    {(node.depth == 1 || node.children == null) && (
                      <rect
                        width={width}
                        height={height}
                        stroke={lightpurple}
                        strokeWidth={10}
                        fill={'transparent'}
                        onClick={e =>
                          {
                            if (node.children == null)
                              return
                            var rem = t =>
                              {
                                var h = t.children
                                var ret = [t.data.data]
                                if (h)
                                  {
                                    return [].concat.apply(ret, h.map(e => rem(e)))
                                  }
                                  {
                                    return ret
                                  }
                              }

                            var new_node = node
                            node.data.data.parentTaskId = null
                            pushCutSet(rem(node))
                        }}

                      onContextMenu={e =>
                        {
                            e.preventDefault()
                            popCutSet()

                        }}
                      />
                    )}



                    {node.depth == 2 && (
                      <rect
                        width={width}
                        height={height}
                        stroke={pink}
                        strokeWidth={10}
                        fill={'transparent'}
                      />
                    )}

                    {node.depth ==2 && (
                      <text
                        dy={'2.33em'}
                        dx={'10em'}
                        fontSize={20}
                        fontFamily="Arial"
                        textAnchor={'middle'}
                        style={{ pointerEvents: 'none' }}
                        fill={pink}
                      >
                        {node.data.data.name}
                      </text>
                    )}

                    {node.depth > 2 && (
                      <rect
                        width={width}
                        height={height}
                        stroke={pink}
                        fill={colorScale(node.value )}
                      />
                    )}

                    {(node.depth == 1 ||
                      (node.children == null && !node.depth==2))&& (
                      <text
                        scaleToFit
                        dy={'1.33em'}
                        dx={'10em'}
                        fontSize={20}
                        fontFamily="Arial"
                        textAnchor={'start'}
                        style={{ pointerEvents: 'none' }}
                        fill={white}
                      >
                        {node.data.data.name}
                      </text>
                    )}
                  </Group>
                );
              })}
            </Group>
          );
        }}
      </Treemap>
    </svg>
  );
})
