import React from 'react';
import { Provider, connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { DateTimePicker } from 'material-ui-pickers';
import { edit_task_req } from '../api';
import { setEditTask, saveEditTask, endDiag } from '../actions';

const mapStateToProps = state => ({
  editTask: state.tree.editTask,
});

const mapDispatchToProps = dispatch => ({
  setEditTask: task => dispatch(setEditTask(task)),
  save: () => dispatch(saveEditTask()),
  endDiag: () => dispatch(endDiag()),
  hideFloat: () => dispatch(endDiag()),
});
const flex = ({
  display: 'flex',
  flexWrap: 'wrap',
});
const EditTaskDialog = props => (
  <div>
    <DialogContent className={flex}>
      <Grid container spacing={24} className={{ flexGrow: 1 }}>
        <Grid item xs={4}>
          <TextField
            className={flex}
            defaultValue={props.editTask.name}
            label="Task name"
            onChange={e => props.setEditTask({ ...props.editTask, name: e.target.value })}
            autoFocus
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            className={flex}
            id="date"
            label="Deadline"
            type="date"
            defaultValue={props.editTask.deadline}

            InputLabelProps={{
              shrink: true,
            }}
            onChange={e => props.setEditTask({ ...props.editTask, deadline: e.target.value })}
          />
        </Grid>
        <Grid item xs={4}>

          <TextField
            id="standard-select-currency"
            select
            label="Status"
            value={props.editTask.status ? props.editTask.status : 'Undefined'}
            onChange={e => props.setEditTask({ ...props.editTask, status: e.target.value })}


            style={{ width: '100px' }}
          >
            {['ToDo', 'In Progress', 'Done', 'Failed', 'Undefined'].map(option => (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            ))}
          </TextField>


        </Grid>
        <Grid item xs={12}>

          <TextField
            className={{ ...flex, margin: 8 }}
            rowsMax="4"
            multiline
            fullWidth
            margin="normal"
            defaultValue={props.editTask.description}
            label="Task Description"
            onChange={e => props.setEditTask({ ...props.editTask, description: e.target.value })}
            autoFocus
          />
        </Grid>
      </Grid>
    </DialogContent>

    <DialogActions>
      <Button
        color="secondary"
        variant="outlined"
        onClick={() => {
          if (props.editTask.name != '') {
            edit_task_req(props.editTask)
              .then((e) => {
                props.setEditTask(e.data);
                props.save();
              })
              .catch(
                (req) => {
                  console.log(req);
                  props.setSnack('server error please try again');
                },
              );
          }
          props.endDiag();
        }}
      >
        Edit Task
      </Button>
      <Button onClick={props.hideFloat} color="primary">
      Cancel
      </Button>
    </DialogActions>
  </div>
);

export default connect(mapStateToProps,
  mapDispatchToProps)(EditTaskDialog);
