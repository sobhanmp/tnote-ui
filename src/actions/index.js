export const setGraph = graph => ({
  type: 'graph',
  data: graph,
});
export const switchDrawer = (open, me) => ({
  type: 'drawer',
  data: open,
  creator: me,
});

export const setSubPage = subPage => ({
  type: 'sub',
  subPage,
});

export const setPage = pageName => ({ type: pageName });

export const setUser = userName => ({
  type: 'setUser',
  data: userName,
});


export const setPass = passWord => ({
  type: 'setPass',
  data: passWord,
});

export const setNode = data => ({
  type: 'setNode',
  data,
});

export const hideFloat = function () {
  return ({
    type: 'endDiag',
  });
};


export const showFloat = parentTaskId => ({
  type: 'show',
  parentTaskId,
});


export const showNew = () => ({ type: 'showNew' });

export const endDiag = () => ({ type: 'endDiag' });

export const setTask = tasks => ({
  type: 'set task',
  data: tasks,
});
export const setNewTask = task => ({
  type: 'set new task',
  data: task,
});

export const setEditTask = task => ({
  type: 'set edit task',
  data: task,
});

export const saveEditTask = () => ({
  type: 'save edit task',
});

export const saveNewTask = () => ({
  type: 'save new task',
});
export const setDiagPage = page => ({ type: 'set diag page', data: page });
export const createNewTask = data => ({ type: 'new task', data });

export const avatar_menu_set_anchor = data => ({ type: 'set avatar anchor', data });

export const saveTask = task => ({ type: 'edit task', data: task });

export const remTask = id => ({ type: 'remove task', data: id });
export const setWinSize = win => ({ type: 'winsize', data: win });

export const pushCutSet = c => ({ type: 'push cutset', data: c });
export const popCutSet = () => ({ type: 'pop cutset' });

export const setSnack = snack => ({ type: 'set snack', data: snack });
