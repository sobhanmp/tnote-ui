import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';


import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import DateFnsUtils from '@date-io/date-fns';
import logo from './logo.svg';

import SignIn from './SignIn';
import SignUp from './SignUp';
import Main from './Main';

import { setPage, setSnack, setUser } from '../actions';
import { set_token, get_user_req } from '../api';
// pick utils

import './App.css';

const mapStateToProps = state => ({
  page: state.app.page,
  snack: state.app.snack,
});


const mapDispatchToProps = dispatch => ({
  setPage: type => dispatch(setPage(type)),
  setSnack: snack => dispatch(setSnack(snack)),

  setUser: e => dispatch(setUser(e)),
});

class App extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    // try
    //   {
    // //   const u = sessionStorage.getItem("token")
    //   if (u)
    //     {
    //       console.log('#################33')
    //       console.log(u)
    //       const p = JSON.parse(u)
    //       console.log(p)

    //       set_token(p);
    //       //get_user_req(p.user).then(req => {

    //         //this.props.dispatchsetPage('main view');
    //         //this.props.setUser(p.user);

    //         //console.log("####333\n\n\n\n\n\n")
    //      // })
    //     }
    //   }
    // catch(e)
    //   {
    //     sessionStorage.setItem("token", "")
    //   }
  }

  render() {
    return (
      <div className="App">
        <Main />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.props.snack != null}
          autoHideDuration={6000}
          onClose={e => this.props.setSnack(null)}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}

          message={(
            <span id="message-id">
              {this.props.snack}
            </span>
)}

          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={e => this.props.setSnack(null)}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />


      </div>
    );
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(App);
