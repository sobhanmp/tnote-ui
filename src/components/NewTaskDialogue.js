import React from 'react';

import { Provider, connect } from 'react-redux';


import Grid from '@material-ui/core/Grid';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { DateTimePicker } from 'material-ui-pickers';
import { add_task_req } from '../api';

import {
  setNewTask, saveNewTask, endDiag, setSnack,
} from '../actions';

const mapStateToProps = state => ({
  newTask: state.tree.newTask,
});

const mapDispatchToProps = dispatch => ({
  setNewTask: task => dispatch(setNewTask(task)),
  save: () => dispatch(saveNewTask()),
  endDiag: () => dispatch(endDiag()),
  hideFloat: () => dispatch(endDiag()),
  setSnack: snack => dispatch(setSnack(snack)),
});
const flex = ({
  display: 'flex',
  flexWrap: 'wrap',
});
const NewTaskDialog = props => (
  <div>
    <DialogContent className={flex}>
      <Grid container spacing={4} className={{ flexGrow: 1 }}>
        <Grid item xs={4}>
          <TextField
            className={flex}
            defaultValue={props.newTask.name}
            label="Task name"
            onChange={e => props.setNewTask({ ...props.newTask, name: e.target.value })}
            autoFocus
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            className={flex}
            id="date"
            label="Deadline"
            type="date"
            defaultValue={props.newTask.deadline}

            InputLabelProps={{
              shrink: true,
            }}
            onChange={e => props.setNewTask({ ...props.newTask, deadline: e.target.value })}
          />
        </Grid>
        <Grid item xs={4}>

          <TextField
            id="standard-select-currency"
            select
            label="Status"
            value={props.newTask.status ? props.newTask.status : 'Undefined'}
            onChange={e => props.setNewTask({ ...props.newTask, status: e.target.value })}


            style={{ width: '100px' }}
            inputStyle={{ width: '100px' }}
          >

            {['ToDo', 'In Progress', 'Done', 'Failed', 'Undefined'].map(option => (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            ))}
          </TextField>


        </Grid>
        <Grid item xs={12}>

          <TextField
            className={{ ...flex, margin: 8 }}
            rowsMax="4"
            multiline
            fullWidth
            margin="normal"
            defaultValue={props.newTask.description}
            label="Task Description"
            onChange={e => props.setNewTask({ ...props.newTask, description: e.target.value })}
            
          />
        </Grid>
      </Grid>
    </DialogContent>

    <DialogActions>
      <Button
        color="secondary"
        variant="outlined"
        onClick={() => {
          if (props.newTask.name != '') {
            add_task_req(props.newTask)
              .then((req) => {
                props.setNewTask(req.data);
                props.save();
              })
              .catch(
                (req) => {
                  console.log(req);
                  props.setSnack('server error please try again');
                },
              );
          }
          props.endDiag();
        }}
      >
        New Task
      </Button>
      <Button onClick={props.hideFloat} color="primary">
      Cancel
      </Button>
    </DialogActions>
  </div>
);

export default connect(mapStateToProps,
  mapDispatchToProps)(NewTaskDialog);
