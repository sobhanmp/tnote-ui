const defaultState = {}


const signin = function(state = defaultState, action){
    switch(action.type) {
      case 'setUser':
        return {...state,user: action.data}
      case 'setPass':
        return {...state, pass: action.data}
      default:
        return state
    }
}


export default signin
