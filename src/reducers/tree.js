/* eslint-disable no-var */

import { stratify } from 'd3-hierarchy';
import Fuse from 'fuse.js';
import data from '../tree_viewer/test_data.json';

const defaultState = {
  data,
  next_id: 20,
  cutSet: [data],
  show: false,
  diagPage: 0,
  newDiag: false,
  parentTaskId: 0,
  strat: stratify().id(d => d.id).parentId(d => d.parentTaskId)(data),
};


const rem = (data, id) => {
  if (id == 0) { return data; }
  const list = [id];
  while (list.length) {
    // eslint-disable-next-line vars-on-top
    var t = list.pop();
    data = data.filter((e) => {
      if (e.parentTaskId == t || e.id == t) {
        list.push(e.id);
        return false;
      }
      return true;
    });
  }
  return data;
};
console.log(data)
const options = ({
  id: 'id',
  shouldSort: true,
  threshold: 0.6,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    'name',
    'tags',
    'shared.name',
  ],
});
const fuse = new Fuse(data, options);

function tree(state = defaultState, action) {
  switch (action.type) {
    case 'setNode':
      return { ...state, node: action.data };

    case 'save new task':
      return {
        ...state,
        data: state.data.concat(state.newTask),
        cutSet: [state.data.concat(state.newTask)],
        next_id: state.next_id + 1,
      };
    case 'set task':
      return { ...state, data: action.data, cutSet: [action.data] };
    case 'save edit task':
      return {
        ...state,
        data:
        state.data.map(e => (e.id == state.editTask.id ? state.editTask : e)),
        cutSet: [state.data.map(e => (e.id == state.editTask.id ? state.editTask : e))],
      };

    case 'remove task':
      const d = rem(state.data, action.data);
      return {
        ...state,
        data: d,
        cutSet: [d],
      };


    case 'push cutset':
      return { ...state, cutSet: state.cutSet.concat([action.data]) };
    case 'pop cutset':
      const newSet = state.cutSet.length > 1
        ? state.cutSet.slice(0, -1)
        : [state.data];
      return { ...state, cutSet: newSet };

    case 'show':
      var currentTask = state.data.filter(e => e.id == action.parentTaskId);
      if (currentTask.length) { currentTask = currentTask[0]; } else { currentTask = null; }
      return {
        ...state,
        show: true,
        parentTaskId: action.parentTaskId,

        editTask: currentTask,
        newTask: { id: state.next_id, parentTaskId: currentTask.id },

      };
    case 'showNew':
      return { ...state, newDiag: true };
    case 'endDiag':
      return {
        ...state,
        newDiag: false,
        show: false,
        newTask: null,
        editTask: null,
      };

    case 'set new task':
      return { ...state, newTask: action.data };

    case 'set edit task':
      return { ...state, editTask: action.data };

    case 'set diag page':
      return { ...state, diagPage: action.data };


    case 'set search':
      return {
        ...state,
        search: action.search,
        filter: action.search ? fuse.search(action.search) : null,
      };

    default:
      return state;
  }
}


export default tree;
