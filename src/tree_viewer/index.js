import * as d3 from 'd3'

const draw = (node, data, showFloat, hideFloat, show, endDiag) => {




// set the dimensions and margins of the diagram
const margin = {top: 40, right: 90, bottom: 50, left: 90},
  height = 700,
  width = 800

// convert table into a treelike structure
const treeData = d3.stratify()
    .id(d => d.id)
    .parentId(d=> {return d.parentTaskId})
    (data)

console.log ("---------tree data------------")
console.log (treeData)

//  assigns the data to a hierarchy using parent-child relationships



var nodes = d3.hierarchy(treeData)

// clean up
d3.select(node).selectAll('*').remove();


const dy = width / (nodes.height + 1),
  dx = 10


const t = d3.cluster(treeData).size([width, height])

const treemap = t
console.log("-----------treemap-----------")
console.log(treemap)
nodes = treemap(nodes);


let x0 = Infinity;
let x1 = -x0;
nodes.each(d => {
  if (d.x > x1) x1 = d.x;
  if (d.x < x0) x0 = d.x;
});
// create svg and a g and posotion it
var svg = d3.select(node)
      .attr("width", width + margin.left + margin.right)
      .attr("height",(x1 - x0) *dx + height + margin.top + margin.bottom)
      .on("click", () => {hideFloat(); endDiag()})
      ,
    g = svg.append("g")
      .attr("transform",
            "translate(" + margin.left + "," + margin.top +
            ")")

// // adds the links between the nodes
const link =  g.selectAll(".link")
    .data( nodes.descendants().slice(1))
  .enter().append("path")
    .attr("class", "link")
    .attr("d", function(d) {
       return "M" + d.y + "," + d.x
        + "C" + (d.y + d.parent.y) /2 + "," + d.x
         + " " + (d.y + d.parent.y)/2 + "," + d.parent.x
         + " " + d.parent.y + "," + d.parent.x;
       })
    .attr('fill', '#fff')
    .attr('stroke', "steelblue")
    .attr("stroke-width", "2px")
//
// // adds each node as a group
var node = g.selectAll(".node")
    .data(nodes.descendants())
  .enter().append("g")
    .attr("class", d => "node" +
        (d.children ? " node--internal" : " node--leaf") )
    .attr("transform", d =>
      "translate(" + d.y + "," + d.x + ")")
    .on("click", (d) => {
            let p = {x: d.y, y: d.x}
            console.log(d)
            showFloat(d.data.data.id)
            d3.event.stopPropagation()
        }
      )
//
//
// // adds the circle to the node
node.append("circle")
  .attr ("r", 15)
  .attr ("opacity", 0)
  .attr ("fill", "#fff")

node.append("circle")
  .attr("r", 5)
  .attr("fill", "#fff")
  .attr("stroke", "steelblue")
  .attr("stroke-width", "3px")


// // adds the text to the node
node.append("text")
  .attr("dy", "0.31em")
  .attr("x", d => d.children ? -6 : 6)
  .text(function(d) { return d.data.data.name; })
  .filter(d => d.children)
     .attr("text-anchor", "end")
  .attr("font", "12px sans-serif")
  .clone(true).lower()
     .attr("stroke", "white");
}

export default draw
