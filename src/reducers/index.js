
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import app from './app';
import signin from './signin';
import tree from './tree';

export default history => combineReducers({
  app,
  signin,
  tree,
  router: connectRouter(history),
});
