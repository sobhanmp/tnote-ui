import { switchDrawer, setSubPage,
  setGraph } from '../actions'

import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import GraphicEqIcon from '@material-ui/icons/GraphicEq';

const mapStateToProps = state => ({
  open: state.app.open
})

const mapDispatchToProps = dispatch => ({
  setDrawer: open => dispatch (switchDrawer(open, "drawer")),
  setGraph: graph => dispatch (setGraph(graph)),

  setSubPage: subPage => dispatch (setSubPage(subPage)),
  setDrawer: open => dispatch (switchDrawer(open, "main")),
})

function MainDrawer({open, setDrawer, setGraph}) {

  return (
    <div>

      <Drawer
        open={open}

        onClose={() => setDrawer(false)}
      >

      <div
        tabIndex={0}
        role="button"

        onClick={() => setDrawer(false)}
        onKeyDown={() => setDrawer(false)}
      >

        <List>
          {['vx tree', 'vx treemap'].map((text, index) => (
            <ListItem button onClick={() => setGraph(text)} key={text}>

              <ListItemIcon>
                <GraphicEqIcon />
              </ListItemIcon>

              <ListItemText primary={text} />

            </ListItem>
          ))}
        </List>

      </div>
    </Drawer>
  </div>)
}



export default connect
(mapStateToProps,
mapDispatchToProps)(MainDrawer)
