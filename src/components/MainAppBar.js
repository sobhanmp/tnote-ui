import React from 'react';
import { connect } from 'react-redux';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';

import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Typography from '@material-ui/core/Typography';
import { fade } from '@material-ui/core/styles/colorManipulator';
import InputBase from '@material-ui/core/InputBase';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import makeStyles from '@material-ui/styles/makeStyles';
import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';
import { InputAdornment, TextField, Input } from '@material-ui/core';
import {
  switchDrawer,
  avatar_menu_set_anchor,
} from '../actions';

const mapStateToProps = state => ({
  search: state.tree.search,
});

const mapDispatchToProps = dispatch => ({
  setDrawer: open => dispatch(switchDrawer(open, 'main')),
  set_avatar_anchor: data => dispatch(avatar_menu_set_anchor(data)),
  setSearch: search => dispatch(
    ({
      type: 'set search',
      search,
    }),
  ),
});

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  otherButton:
  { marginLeft: 'auto', flex: 1 },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 10,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: theme.spacing.unit,
    paddingLeft: theme.spacing.unit / 2,
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 700,
      },
    },
  },
});
const useStyles = makeStyles(styles);

function MainAppBar(props) {
  const classes = useStyles();
  const { search, setSearch } = props;
  return (
    <AppBar position="sticky">
      <Toolbar>
        <IconButton
          className={classes.menuButton}
          onClick={() => props.setDrawer(true)}
          color="inherit"
          aria-label="Menu"
        >
          <MenuIcon />
        </IconButton>

        <Typography className={classes.title} variant="h3" color="inherit" noWrap>
            TTASK
        </Typography>
        <div className={classes.search}>

          <Input
            startAdornment={(
              <InputAdornment position="start">
                <SearchIcon className={classes.searchIcon} />
              </InputAdornment>
            )}
            endAdornment={search && (
              <InputAdornment position="end">
                <CloseIcon onClick={() => setSearch('')} />
              </InputAdornment>
            )}

            placeholder="Search…"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            onChange={e => setSearch(e.target.value)}
          />
        </div>
        <div className={classes.grow} />

        <IconButton
          className={classes.otherButton}
          aria-owns="menu-appbar"
          aria-haspopup="true"
          color="inherit"
          onClick={e => props.set_avatar_anchor(e.currentTarget)}
        >


          <AccountCircle />

        </IconButton>

      </Toolbar>

    </AppBar>
  );
}

export default connect(mapStateToProps,
  mapDispatchToProps)(MainAppBar);
