import React, { useState } from 'react';
import Paper from '@material-ui/core/Paper';
import Input from '@material-ui/core/Input';
import { makeStyles } from '@material-ui/styles';
import {
  Column,
  FilteringState, GroupingState,
  IntegratedFiltering, IntegratedGrouping, IntegratedPaging, IntegratedSelection, IntegratedSorting,
  PagingState, SelectionState, SortingState, DataTypeProvider, DataTypeProviderProps,
} from '@devexpress/dx-react-grid';
import {
  DragDropProvider,
  Grid, GroupingPanel, PagingPanel,
  Table, TableFilterRow, TableGroupRow,
  TableHeaderRow, TableSelection, VirtualTable,
} from '@devexpress/dx-react-grid-material-ui';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import MessageIcon from '@material-ui/icons/Message';
import PauseIcon from '@material-ui/icons/Pause';
import data from '../mockData/users.json';

const useStyle = makeStyles(theme => ({
  currency: {
    fontWeight: theme.typography.fontWeightMedium,
  },
  numericInput: {
    width: '100%',
  },
}));
const cols = [
  { name: 'id', title: 'ID' },
  { name: 'firstName', title: 'First Name' },
  { name: 'lastName', title: 'Last Name' },
  { name: 'email', title: 'Email' },
  { name: 'activityScore', title: 'Activity Score' },
  { name: 'creationDate', title: 'Creation Date' },
  { name: 'icon', title: 'Actions' }];

const pageSizes = [5, 10, 15];
const getRowId = row => row.id;

const rows = data.map(e => ({
  ...e,
  icon:
  <div>
    <IconButton>
      <DeleteIcon />
    </IconButton>
    <IconButton>
      <MessageIcon />
    </IconButton>
    <IconButton>
      <PauseIcon />
    </IconButton>
  </div>,
}));
const getColor = (amount) => {
  if (amount < 25) {
    return '#F44336';
  }
  if (amount < 50) {
    return '#FFC107';
  }
  if (amount < 75) {
    return '#FF5722';
  }
  return '#009688';
};
const getInputValue = value => (value === undefined ? '' : value);
const availableFilterOperations = [
  'equal', 'notEqual',
  'greaterThan', 'greaterThanOrEqual',
  'lessThan', 'lessThanOrEqual',
];

const CurrencyEditor = ({ onValueChange, value }) => {
  const classes = useStyle();
  const handleChange = (event) => {
    const { value: targetValue } = event.target;
    if (targetValue.trim() === '') {
      onValueChange(undefined);
      return;
    }
    onValueChange(parseInt(targetValue, 10));
  };
  return (
    <Input
      type="number"
      classes={{
        input: classes.numericInput,
      }}
      fullWidth
      value={getInputValue(value)}
      inputProps={{
        min: 0,
        placeholder: 'Filter...',
      }}
      onChange={handleChange}
    />
  );
};

const CurrencyFormatter = ({ value }) => {
  const classes = useStyle();
  return (<i className={classes.currency} style={{ color: getColor(value) }}>{value}</i>);
};

const CurrencyTypeProvider = props => (
  <DataTypeProvider
    formatterComponent={CurrencyFormatter}
    editorComponent={CurrencyEditor}
    availableFilterOperations={availableFilterOperations}
    {...props}
  />
);

export default function (props) {
  const [selection, setSelection] = useState([]);

  return (
    <Paper>
      <AppBar>
        <Toolbar>
          hi
        </Toolbar>
      </AppBar>
      <AppBar />
      <Grid
        rows={rows}
        columns={cols}
        getRowId={getRowId}
      >
        <SelectionState
          selection={selection}
          onSelectionChange={setSelection}
        />
        <FilteringState
          defaultFilters={[]}
          columnExtensions={[{ columnName: 'icon', filteringEnabled: false }]}
        />
        <SortingState
          defaultSorting={[{ columnName: 'email', direction: 'asc' }]}
        />
        <IntegratedSelection />

        <IntegratedFiltering />
        <IntegratedSorting />
        <CurrencyTypeProvider for={['activityScore']} />
        <VirtualTable />
        <TableHeaderRow showSortingControls />
        <TableFilterRow showFilterSelector />
        <TableSelection showSelectAll />
      </Grid>
    </Paper>
  );
}
