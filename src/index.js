import './bootstrap.js';
/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { ThemeProvider } from '@material-ui/styles';
import whyDidYouRender from '@welldone-software/why-did-you-render';
import whyDidYouUpdate from 'why-did-you-update';
import configureStore, { history } from './configureStore';
import App from './components/App';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import Landing from './components/Landing';
import theme from './theme/theme';
import AdminLogin from './components/AdminLogin';
import AdminDashBoard from './components/AdminDashBoard';

const store = configureStore();
// whyDidYouRender(React, { include: [/^.*/] });

ReactDOM.render(
  <Provider store={store}>
    <>
      {/* router */}
      <ThemeProvider theme={theme}>
        <ConnectedRouter history={history}>
          <Switch>

            <Route path="/" exact component={Landing} />
            <Route path="/app" component={App} />
            <Route path="/signin" component={SignIn} />
            <Route path="/signup" component={SignUp} />
            <Route path="/Admin" exact component={AdminLogin} />
            <Route path="/Admin/dashboard" component={AdminDashBoard} />

          </Switch>
        </ConnectedRouter>
      </ThemeProvider>
    </>
  </Provider>,
  document.getElementById('root'),
);
// <Route path="/404" render={PageNotFound} />
/* <Route render={PageNotFound} />  */
//
