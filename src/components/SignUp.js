import React, { useState } from 'react';
import { connect } from 'react-redux';

import {
  AppBar, Avatar, TextField, Button, Paper, Toolbar, Typography, CssBaseline, withStyles,
} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

import { push } from 'connected-react-router';

import LoginForm from './LoginForm';
import styles from '../theme/login';
import { signup_req } from '../api';
import {
  setUser, setPass, setSnack,
} from '../actions';

const mapStateToProps = null;

const mapDispatchToProps = dispatch => ({
  gotoSignIn: () => dispatch(push('/signin')),
  setSnack: snack => dispatch(setSnack(snack)),
});


function SignUp({ gotoSignIn, setSnack }) {
  const signup = (user, pass) => signup_req(user, pass)
    .then(() => gotoSignIn())
    .catch((e) => { console.log(e); setSnack('Error Sign in'); });

  return (
    <LoginForm
      appbar={{ text: 'Sign In' }}
      firstButton={{
        text: 'Already Have an Account? Sign In',
        action: gotoSignIn,
      }}

      firstText={{
        label: 'user name',
        hint: 'Enter you Username',
      }}

      secondText={{
        label: 'password',
        hint: 'Enter your password',
      }}

      secondButton={{
        text: 'Sign Up',
        action: signup,
      }}
    />
  );
}


export default connect(mapStateToProps,
  mapDispatchToProps)(withStyles(styles)(SignUp));
