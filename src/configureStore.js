import { applyMiddleware, compose, createStore } from 'redux'
import createRootReducer from './reducers'
import { routerMiddleware } from 'connected-react-router'
import { logger } from 'redux-logger'
import { createBrowserHistory } from 'history'



export const history = createBrowserHistory()

export default function configureStore (preloadedState) {
  const store = createStore(
    createRootReducer(history),
    preloadedState,
    compose(
      applyMiddleware(
        routerMiddleware(history),
        logger)))
  
  return store
} 
