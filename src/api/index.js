import crypto from 'crypto-js'
import axios from 'axios'
require('axios-debug')(axios);
const api_root = 'http://192.168.196.69:8080'

const api_user = api_root + '/user'

const api_signup = api_user + '/signup'
const api_signin = api_user + '/signin'


const api_task = api_root + '/task'

const api_add = api_task + '/add'
const api_rem = api_task + '/remove'
const api_edit = api_task + '/edit'
var token = null
var id = null
export const signup_req = (user, pass) =>
  axios.post(
    api_signup,
    {
      userName: user,
      hashedPassword: crypto.SHA512(pass).toString()
    }
  )

export const signin_req = (user, pass) =>
  axios.post(
    api_signin,
    {
      userName: user,
      hashedPassword: crypto.SHA512(pass).toString()
    }
  )

export const add_task_req = task => 
  axios.post(
    api_add,
    task,
    {headers: {token: token}})

export const rem_task_req = taskId =>
    axios.get(
      api_rem + '/' + taskId,
      {headers: {token: token, userId: id}}
    )
export const edit_task_req = task =>
    axios.post(
      api_edit,
      task,
      {headers: {token: token, userId: id}}
    )
export const get_user_req = user =>
  axios.get(
    api_user + '/' + id,
    {headers: {token: token}})


export const set_token = t => {token = t.token; id = t.id}

export const adminSignInReq = (user, pass) => null