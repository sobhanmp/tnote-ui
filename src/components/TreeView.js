import React from 'react';
import { Group } from '@vx/group';
import { Tree } from '@vx/hierarchy';
import { LinkHorizontal } from '@vx/shape';
import { hierarchy, stratify} from 'd3-hierarchy';
import { LinearGradient } from '@vx/gradient';

import { connect } from 'react-redux'

import { showFloat, hideFloat,
  endDiag } from '../actions'

const peach = '#fd9b93';
const pink = '#fe6e9e';
const blue = '#03c0dc';
const green = '#26deb0';
const plum = '#71248e';
const lightpurple = '#374469';
const white = '#ffffff';
const bg = '#272b4d';

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => ({
  showFloat: par =>
    dispatch (showFloat( par)),

  hideFloat: () => dispatch(hideFloat()),

  endDiag: () => dispatch(endDiag()),

})
const Node = connect
  (mapStateToProps,
  mapDispatchToProps)(({ node, showFloat }) => {
    const width = 40;
    const height = 20;
    const centerX = -width / 2;
    const centerY = -height / 2;
    const isRoot = node.depth === 0;
    const isParent = !!node.children;

    if (isRoot) return <RootNode node={node} />;
    if (isParent) return <ParentNode node={node} />;

    return (
      <Group top={node.x} left={node.y}>
        <rect
          height={height}
          width={Math.max(
          width, 5 * node.data.data.name.substring(0,30).length)}
          y={centerY}
          x={ (5 * node.data.data.name.substring(0,30).length) > width?
            (-5 * node.data.data.name.substring(0,30).length) / 2:
            centerX
          }
          fill={bg}
          stroke={green}
          strokeWidth={1}
          strokeDasharray={'2,2'}
          strokeOpacity={0.6}
          rx={10}
          onClick={() => {
              showFloat(node.data.data.id)
            // d3.event.stopPropagation()
          }
          }
        />
        <text
          dy={'.33em'}
          fontSize={9}
          fontFamily="Arial"
          textAnchor={'middle'}
          fill={green}
          style={{ pointerEvents: 'none' }}
        >
          {node.data.data.name.substring(0,30)}
        </text>
      </Group>
    );
  })

const RootNode = connect
  (mapStateToProps,
  mapDispatchToProps)(({ node, showFloat }) => {
    const width = 40;
    const height = 20;
    const centerX = -width / 2;
    const centerY = -height / 2;
  return (
  <Group top={node.x} left={node.y +40}>

        <rect
          height={height}
          width={Math.max(
            width, 5 * node.data.data.name.substring(0,30).length)}
          y={centerY}
          x={ (5 * node.data.data.name.substring(0,30).length) > width?
              (-5 * node.data.data.name.substring(0,30).length) / 2:
              centerX
            }
          fill={pink}
          stroke={plum}
          strokeWidth={1}
      onClick={() => {
        console.log(node)
        showFloat(node.data.data.id)
       // d3.event.stopPropagation()
    }} />
      <text
      dy={'.33em'}
      fontSize={9}
      fontFamily="Arial"
      textAnchor={'middle'}
      fill={white}
      style={{ pointerEvents: 'none' }}
      >
        {node.data.data.name.substring(0,30).substring(0,30)}
      </text>
    </Group>
  );
})

const ParentNode = connect
(mapStateToProps,
mapDispatchToProps)(({ node, showFloat }) =>{
  const width = 40;
  const height = 20;
  const centerX = -width / 2;
  const centerY = -height / 2;

  return (
    <Group top={node.x} left={node.y}>
      <rect
        height={height}
        width={Math.max(
          width, 5 * node.data.data.name.substring(0,30).length)}
        y={centerY}
        x={ (5 * node.data.data.name.substring(0,30).length) > width?
            (-5 * node.data.data.name.substring(0,30).length) / 2:
            centerX
          }
        fill={bg}
        stroke={blue}
        strokeWidth={1}
        onClick={() => {
          console.log(node)
          showFloat(node.data.data.id)
         // d3.event.stopPropagation()
      }
        }
      />
      <text
        dy={'.33em'}
        fontSize={9}
        fontFamily="Arial"
        textAnchor={'middle'}
        style={{ pointerEvents: 'none' }}
        fill={white}
      >
        {node.data.data.name.substring(0,30)}
      </text>
    </Group>
  );
})

export default connect
  (mapStateToProps,
  mapDispatchToProps)(({
    datum,
    width,
    height,
    hideFloat,
    endDiag,
    margin = {
      top: 10,
      left: 30,
      right: 40,
      bottom: 80
    }
  }) => {
    if (datum == null || datum.length == 0)
     return (<div></div>)
    
    const data = hierarchy(datum);
    const yMax = height - margin.top - margin.bottom;
    const xMax = width - margin.left - margin.right;

    return (
      <svg width={width} height={height}
        >
        <LinearGradient id="lg"
        from={peach} to={pink}
         />
        <rect width={width}
          height={height}
          rx={14} fill={bg}
          onClick = {() =>
            {
              hideFloat();
              endDiag();
            }
          }
          />
        <Tree root={data} size={[yMax, xMax]}>
          {tree => {
            return (
              <Group top={margin.top} left={margin.left}>
                {tree.links().map((link, i) => {
                  return (
                    <LinkHorizontal
                      key={`link-${i}`}
                      data={link}
                      stroke={lightpurple}
                      strokeWidth="1"
                      fill="none"
                    />
                  );
                })}
                {tree.descendants().map((node, i) => {
                  return <Node key={`node-${i}`} node={node}
                   showFloat={showFloat} />;
                })}
              </Group>
            );
          }}
        </Tree>
      </svg>
    );
  })
