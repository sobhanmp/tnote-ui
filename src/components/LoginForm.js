import React, { useState } from 'react';

import {
  AppBar, Avatar, TextField, Button, Paper, Toolbar, Typography, CssBaseline,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

import styles from '../theme/login';

const useStyles = makeStyles(styles);
function LoginForm({
  firstButton, secondButton, firstText, secondText, appbar,
}) {
  const [first, setFirst] = useState(null);
  const [second, setSecond] = useState(null);
  const classes = useStyles();
  return (
    <main className={classes.main}>
      <CssBaseline />
      {appbar && (
      <AppBar position="static">
        <Toolbar>
          <Typography
            variant="h6"
            color="inherit"
            className={{ flexGrow: 1 }}
          >
            {appbar.text}
          </Typography>
        </Toolbar>
      </AppBar>
      )}

      <Paper className={classes.paper} elevation={5}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <form className={classes.form}>
          {firstButton && (
          <Button
            className={classes.button}
            color="secondary"
            onClick={firstButton.action}
            fullWidth
          >
            {firstButton.text}
          </Button>
          )}

          {firstText && (
          <TextField
            label={firstText.label}
            hintText={firstText.hint}
            floatingLabelText={firstText.label}
            fullWidth
            onChange={event => setFirst(event.target.value)}
          />
          )}

          {secondText && (
          <TextField
            type="password"
            label={secondText.label}
            hintText={secondText.hint}
            floatingLabelText={secondText.label}
            margin="normal"
            fullWidth
            onChange={event => setSecond(event.target.value)}
          />
          )}

          {secondButton && (
          <Button
            variant="contained"
            color="primary"
            primary
            className={classes.button}
            fullWidth
            onClick={() => secondButton.action(first, second)}
          >
            {secondButton.text}
          </Button>
          )}
        </form>

      </Paper>

    </main>
  );
}


export default LoginForm;
