import React from 'react';
import { connect } from 'react-redux';

import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import ChipInput from 'material-ui-chip-input';
import WorkIcon from '@material-ui/icons/WorkOutline';
import SyncIcon from '@material-ui/icons/Sync';
import PowerIcon from '@material-ui/icons/PowerSettingsNew';
import ShareIcon from '@material-ui/icons/Share';
import { Button } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import TreeView from './TreeView';
import TreeMapVX from './TreeMapVX';


const mapStateToProps = state => ({
  graph: state.app.graph,
  winSize: state.app.winSize,
  data: state.tree.strat,
  flat: state.tree.data,
  filter: state.tree.filter,
});
const mapDispatchToProps = () => ({

});

const flatApply = (tree, filter, func, h = 0) => {
  let t = [];
  if (!tree) { return []; }
  if (!(filter == null)) {
    const i = filter.findIndex(e => e == tree.id);
    if (i != -1) 
      { 
        t = [{ index: i, data: func(tree) }]; 
      }
  } else {
    if (tree.data.show != 0)
      t = [({ index: h, data: func(tree) })];
  }
  console.log(t[0])
  if (!tree.children) { return t; }

  const children = tree.children.map(e => flatApply(e, filter, func, h + 1));
  return [].concat.apply(t, children);
};

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  chip: {
    margin: theme.spacing.unit / 2,
  },
});

function getIcon(type) {
  switch (type) {
    case 'slave':
      return <WorkIcon />;
    case 'watch':
      return <SyncIcon />;
    case 'master':
      return <PowerIcon />;
    default:
      return null;
  }
}


function GraphViewer({
  winSize, graph, classes, data, filter,
}) {
  console.log(filter);
  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        {flatApply(data, filter, e => (
          <Grid item xs={6} key={e.id}>
            <Card className={classes.paper}>
              <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                  {e.data.name}
                </Typography>
                <TreeView
                  datum={e}
                  width={600}
                  height={700}
                />
              </CardContent>
              <CardActions>
                <Grid container spacing={16}>
                  <Grid item xs={12}>
                    <ChipInput

                      defaultValue={e.data.tags}
                    />
                  </Grid>
                  {e.data.shared
                    && (
                    <Grid item xs={12}>
                      {e.data.shared.map(s => (
                        <Chip
                          key={s.name}
                          label={s.name}
                          icon={getIcon(s.type)}
                          onDelete={() => null}
                          className={classes.chip}
                        />
                      ))
                      }
                    </Grid>
                    )}
                  <Grid item xs={12}>

                    <Button>
                      <ShareIcon />
                    </Button>
                    <Button>
                      <DeleteIcon />
                    </Button>

                  </Grid>
                </Grid>
              </CardActions>
            </Card>
          </Grid>
        )).sort((x, y) => x.index - y.index).map(e => e.data)}


        {graph === 'vx treemap'
          && (
            <Paper className={classes.paper}>
              <TreeMapVX
                width={winSize.width - 100}
                height={winSize.height}
              />

            </Paper>
          )}

      </Grid>
    </div>
  );
}


export default connect(mapStateToProps,
  mapDispatchToProps)(withStyles(styles)(GraphViewer));
