import React, { useState } from 'react';
import { connect } from 'react-redux';


import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import lifecycle from 'react-pure-lifecycle';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import styles from '../theme';
import draw from '../tree_viewer';
import
{
  setPage, setNode,
  showFloat,
  showNew, endDiag, createNewTask,
  switchDrawer, avatar_menu_set_anchor,
  setDiagPage, saveTask,
  remTask, setWinSize, pushCutSet,
  popCutSet, setSnack, setTask,
} from '../actions';

import { get_user_req, add_task_req, rem_task_req } from '../api';

import MainDrawer from './MainDrawer';
import MainAppBar from './MainAppBar';
import EditTaskDialogue from './EditTaskDialogue';
import NewTaskDialogue from './NewTaskDialogue';
import GraphViewer from './GraphViewer';

const mapStateToProps = state => ({


  user: state.signin.user,
  pass: state.signin.pass,

  page: state.app.page,
  graph: state.app.graph,
  node: state.tree.node,

  show: state.tree.show,

  avatar_anchor: state.app.avatarAnchor,
  showPos: state.tree.pos,
  open: state.app.open,

  newDiag: state.tree.newDiag,
  newTask: state.tree.newTask,
  remTaskid: state.tree.parentTaskId,

  drawerOpen: state.app.open,
  diagPage: state.tree.diagPage,
  currentTask: state.tree.data.filter(e => e.id == state.tree.parentTaskId),
  winSize: state.app.winSize,
  cutSet: state.tree.cutSet,
});

const mapDispatchToProps = dispatch => ({
  set_avatar_anchor: data => dispatch(avatar_menu_set_anchor(data)),
  signedOut: () => dispatch(setPage('sign in')),
  writeNode: data => dispatch(setNode(data)),

  showFloat: par => dispatch(showFloat(par)),
  hideFloat: () => dispatch(endDiag()),

  showNew: () => dispatch(showNew()),
  endDiag: () => dispatch(endDiag()),


  createNewTask: (n) => { dispatch(createNewTask(n)); },
  setTask: t => dispatch(setTask(t)),

  setDrawer: open => dispatch(switchDrawer(open, 'main')),

  setDiagPage: page => dispatch(setDiagPage(page)),
  saveTask: task => dispatch(saveTask(task)),
  remTask: id => dispatch(remTask(id)),
  setWinSize: win => dispatch(setWinSize(win)),

  pushCutSet: c => dispatch(pushCutSet(c)),
  popCutSet: () => dispatch(popCutSet()),

  setSnack: snack => dispatch(setSnack(snack)),
});


function Main(props) {
  return (

    <div>


      <Menu
        id="avatar_menu"
        anchorEl={props.avatar_anchor}
        open={Boolean(props.avatar_anchor)}
        onClose={function () { props.set_avatar_anchor(null); }}
      >
        <MenuItem onClick={function () {
          sessionStorage.setItem('token', '');
          props.set_avatar_anchor(null);
        }}
        >
          Sign out
        </MenuItem>
      </Menu>
      <Menu
        id="action_menu"
        anchorEl={props.action_anchor}
        open={Boolean(props.avatarAnchor)}
        onClose={() => props.setActionAnchor(null)}
      />

      <MainAppBar />

      <MainDrawer />
      <br />

      <GraphViewer />
      {props.show && (
      <Dialog
        fullWidth
        maxWidth="md"
        open={props.show}
        onClose={props.hideFloat}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Edit</DialogTitle>
        <Tabs
          value={props.diagPage}
          indicatorColor="primary"
          textColor="primary"
          onChange={(e, v) => { console.log(v); props.setDiagPage(v); }}
        >


          <Tab label="Edit Task" />
          <Tab label="New Task" />
          <Tab label="Delet Task" />
        </Tabs>
        {props.diagPage == 0 && <EditTaskDialogue />}
        {props.diagPage == 1 && <NewTaskDialogue />}


        {props.diagPage == 2 && (
        <DialogActions>
          <Button
            variant="outlined"
            onClick={() => {
                  rem_task_req(props.remTaskid)
                    .then(
                      () => {
                        props.remTask(props.remTaskid);
                        props.endDiag();
                      },

                    )
                    .catch((req) => { console.log(req); props.setSnack('server error please try again'); });
                }}
          >
                    Remove Task With ALL children
          </Button>
        </DialogActions>
        )
              }
      </Dialog>
      )}


    </div>


  );
}

const mydraw = props => (props.graph == 'd3 tree' && draw(
  props.node,
  props.data,
  props.showFloat,
  props.hideFloat,
  props.show,
  props.endDiag,
));

const getWin = () => ({ width: window.innerWidth, height: window.innerHeight });
const componentDidMount = (props) => {
  console.log('##################');
  get_user_req(props.user).then((user) => {
    console.log(user);
    if (!user.data.tasks.length) {
      add_task_req({
        name: 'parentTaskId task',
        description: 'this is the parentTaskId task click edit to edit',
      }).then(req => props.setTask([{ ...req.data, parentTaskId: null }]))
        .catch((req) => { console.log(req); props.setSnack('server error please try again'); });
    } else {
      props.setTask(user.data.tasks.map(e => (e.parentTaskId == '0' ? { ...e, parentTaskId: null } : e)));
    }
  })
    .catch((req) => { console.log(req); props.setSnack('server error please try again'); });
  mydraw(props);
  window.addEventListener('resize', () => props.setWinSize(getWin()));
};

const componentDidUpdate = (props) => {
  mydraw(props);
};
const componentWillReceiveProps = (props) => {
  mydraw(props);
};
const componentWillUnmount = (props) => {
  window.addEventListener('resize', () => props.setWinSize(getWin()));
};
const methods = {
  componentDidMount,
  componentDidUpdate,
  componentWillReceiveProps,
  componentWillUnmount,
};
export default connect(mapStateToProps,
  mapDispatchToProps)(
  lifecycle(methods)(withStyles(styles)(Main)),
);
