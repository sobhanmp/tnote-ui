import React from 'react';

import { push } from 'connected-react-router';
import { connect } from 'react-redux';

import LoginForm from './LoginForm';


import { set_token, adminSignInReq } from '../api';
import { setSnack } from '../actions';


const mapStateToProps = null;

const mapDispatchToProps = dispatch => ({
  signedIn() { dispatch(push('/admin')); },
  gotoUserSignIp() { dispatch(push('/signip')); },
  gotoAdminDashBoard() { dispatch(push('/admin/dashboard'))},
  setSnack: snack => dispatch(setSnack(snack)),
});


function AdminLogin(props) {
  // const signin = (user, pass) => adminSignInReq(user, pass)
  //   .then((e) => {
  //     console.log(e);
  //     set_token(e.data);
  //     sessionStorage.setItem('token', JSON.stringify({ ...e.data, user: props.user }));
  //     props.signedIn();
  //   })
  //   .catch((e) => { console.log(e); props.setSnack('Error Sign in'); });
  const signin = () => props.gotoAdminDashBoard()
  return (
    <LoginForm
      appbar={{ text: 'Admin Sign In' }}
      firstButton={{
        text: 'Not an admin?? goto SignIn',
        action: props.gotoUserSignIn,
      }}

      firstText={{
        label: 'user name',
        hint: 'Enter you Username',
      }}

      secondText={{
        label: 'password',
        hint: 'Enter your password',
      }}

      secondButton={{
        text: 'Sign in',
        action: signin,
      }}
    />
  );
}


export default connect(mapStateToProps, mapDispatchToProps)(AdminLogin);
