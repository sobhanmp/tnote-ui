import {
  CssBaseline, AppBar, Toolbar, Typography, Button,
} from '@material-ui/core';
import React from 'react';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
    color: theme.palette.common.white,
  },
  menuButton: {
    color: theme.palette.common.white,
  },
});
// eslint-disable-next-line no-shadow
function Landing({ classes, push }) {
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="static" className={classes.root}>

        <Toolbar>
          <Typography variant="h6" className={classes.grow}>
          TTask
          </Typography>
          <Button onClick={() => push('/signin')} className={classes.menuButton}>
            Sign In
          </Button>

          <Button onClick={() => push('/signup')} className={classes.menuButton}>
            Sign Up
          </Button>
        </Toolbar>
      </AppBar>
      <main>
        <div>
          <Typography
            component="h1"
            variant="h2"
            align="center"
            color="textPrimary"
            gutterBottom
          >
            TreeLike Task Manager
          </Typography>
          <Typography variant="h6" align="center" color="textSecondary" component="p">
            Clearly manages your task in our new and efficient(?) task manager
          </Typography>
        </div>
      </main>
    </React.Fragment>
  );
}

export default connect(null, { push })(withStyles(styles)(Landing));
