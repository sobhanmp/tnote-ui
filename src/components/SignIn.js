import React from 'react';

import { push } from 'connected-react-router';
import { connect } from 'react-redux';

import LoginForm from './LoginForm';


import { set_token, signin_req } from '../api';
import { setSnack } from '../actions';


const mapStateToProps = null;

const mapDispatchToProps = dispatch => ({
  signedIn() { dispatch(push('/app')); },
  gotoSignUp() { dispatch(push('/signup')); },
  setSnack: snack => dispatch(setSnack(snack)),
});


function SignIn(props) {
  const signin = (user, pass) => signin_req(user, pass)
    .then((e) => {
      console.log(e);
      set_token(e.data);
      sessionStorage.setItem('token', JSON.stringify({ ...e.data, user: props.user }));
      props.signedIn();
    })
    .catch((e) => { console.log(e); props.setSnack('Error Sign in'); });
  return (
    <LoginForm
      appbar={{ text: 'Sign In' }}
      firstButton={{
        text: 'Don\'t have an account?? signup',
        action: props.gotoSignUp,
      }}

      firstText={{
        label: 'user name',
        hint: 'Enter you Username',
      }}

      secondText={{
        label: 'password',
        hint: 'Enter your password',
      }}

      secondButton={{
        text: 'Sign in',
        action: signin,
      }}
    />
  );
}


export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
