// const defaultState = { page:'sign in', open: false}
const defaultState = {
  page: 'sign in', open: false, graph: 'vx tree', winSize: { width: 1000, height: 800 },
};

function app(state = defaultState, action) {
  switch (action.type) {
    case 'sign in':
      return { ...state, page: 'sign in', snackbar: null };
    case 'sign up':
      return { ...state, page: 'sign up', snackbar: null };
    case 'intro page':
      return { ...state, page: 'intro', snackbar: null };
    case 'main view':
      return { ...state, page: 'main', snackbar: null };
    case 'settings':
      return { ...state, page: 'settings', snackbar: null };
    case 'drawer':
      return { ...state, open: action.data };
    case 'graph':
      return { ...state, graph: action.data };
    case 'winsize':
      return { ...state, winSize: action.data };
    case 'set avatar anchor':
      return { ...state, avatarAnchor: action.data };
    case 'set snack':
      return { ...state, snack: action.data };
    default:
      return state;
  }
}


export default app;
